package br.com.caelum.camel;

import java.util.Calendar;

public class Negociacao {
	private Double preco;
	private Integer quantidade;
	private Calendar data;
	
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "Negociacao [preco=" + preco + ", quantidade=" + quantidade + "]";
	}
}
